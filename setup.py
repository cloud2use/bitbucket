
from setuptools import setup, find_packages

setup(
    name='amazon 2use',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='Cloud specs for Amazon',
    long_description=open('README.txt').read(),
    install_requires=[
        'numpy',
    ],
    url='https://github.com/cloud2use/amazon',
    author='TAYAA Med Amine',
    author_email='amazon@it-issal.cloud'
)
